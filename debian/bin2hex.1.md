% BIN2HEX(1) bin2hex Man Page
% Philippe Thierry
% January 2019


# NAME
bin2hex - binary to Intel Hex converter utility

# SYNOPSIS
**bin2hex [options] INFILE [OUTFILE]**

# DESCRIPTION

This tool is a part of a oolset for manipulating Intel HEX file format
The Intel HEX file format is widely used in microprocessors and
microcontrollers  area (embedded systems etc) as the de facto standard for
representation of code to be programmed into microelectronic devices.

Associated toolset content:

 - bin2hex - binary to Intel Hex converter utility
 - hex2bin - IntelHex to binary converter utility
 - hex2dump - show content of hex file as hexdump
 - hexdiff - diff dumps of 2 hex files
 - hexinfo - diff dumps of 2 hex files
 - hexmerge -  merge content of hex files

# OPTIONS

Arguments:

*INFILE*
name of bin file for processing. Use '-' for reading from stdin.

*OUTFILE*
name of output file. If omitted then output will be writing to stdout.

**-h, --help**
Display simple help message describing the tool usage.

**-v, --version**
Display bin2hex tool version information

**--offset=N**
offset for loading bin file (default: 0).


# HISTORY

January 2019, Man page originally compiled by Philippe Thierry (phil at debian dot org)
