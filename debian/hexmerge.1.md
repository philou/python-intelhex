% HEXMERGE(1) hexmerge Man Page
% Philippe Thierry
% January 2019


# NAME
hexmerge - merge content of hex files

# SYNOPSIS
**hexmerge [options] FILES..**

# DESCRIPTION

This tool is a part of a oolset for manipulating Intel HEX file format
The Intel HEX file format is widely used in microprocessors and
microcontrollers  area (embedded systems etc) as the de facto standard for
representation of code to be programmed into microelectronic devices.

Associated toolset content:

 - bin2hex - binary to Intel Hex converter utility
 - hex2bin - IntelHex to binary converter utility
 - hex2dump - show content of hex file as hexdump
 - hexdiff - diff dumps of 2 hex files
 - hexinfo - diff dumps of 2 hex files
 - hexmerge -  merge content of hex files

# OPTIONS

Arguments:

*FILES*
list of hex files for merging (use '-' to read content from stdin)

**-h, --help**
Display simple help message describing the tool usage.

**-v, --version**
Display hexdiff tool version information

**-o, --output=FILENAME**
output file name (emit output to stdout if option is not specified)

**-r, --range=START:END**
specify address range for output (ascii hex value). Both values are inclusive.
Range can be in form 'START:' or ':END'.

**--no-start-addr**
Don't write start addr to output file.

**--overlap=METHOD**
What to do when data in files overlapped. Supported variants:
   * error -- stop and show error message (default)
   * ignore -- keep data from first file that contains data at overlapped address
   * replace -- use data from last file that contains data at overlapped address

# HISTORY

January 2019, Man page originally compiled by Philippe Thierry (phil at debian dot org)
