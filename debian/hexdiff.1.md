% HEXDIFF(1) hexdiff Man Page
% Philippe Thierry
% January 2019


# NAME
hexdiff - diff dumps of 2 hex files

# SYNOPSIS
**hexdiff [options] HEXFILE**

# DESCRIPTION

This tool is a part of a oolset for manipulating Intel HEX file format
The Intel HEX file format is widely used in microprocessors and
microcontrollers  area (embedded systems etc) as the de facto standard for
representation of code to be programmed into microelectronic devices.

Associated toolset content:

 - bin2hex - binary to Intel Hex converter utility
 - hex2bin - IntelHex to binary converter utility
 - hex2dump - show content of hex file as hexdump
 - hexdiff - diff dumps of 2 hex files
 - hexinfo - diff dumps of 2 hex files
 - hexmerge -  merge content of hex files

# OPTIONS

**-h, --help**
Display simple help message describing the tool usage.

**-v, --version**
Display hexdiff tool version information

# HISTORY

January 2019, Man page originally compiled by Philippe Thierry (phil at debian dot org)
