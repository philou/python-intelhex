% HEX2DUMP(1) hex2dump Man Page
% Philippe Thierry
% January 2019


# NAME
hex2dump - how content of hex file as hexdump

# SYNOPSIS
**hex2dump [options] HEXFILE**

# DESCRIPTION

This tool is a part of a oolset for manipulating Intel HEX file format
The Intel HEX file format is widely used in microprocessors and
microcontrollers  area (embedded systems etc) as the de facto standard for
representation of code to be programmed into microelectronic devices.

Associated toolset content:

 - bin2hex - binary to Intel Hex converter utility
 - hex2bin - IntelHex to binary converter utility
 - hex2dump - show content of hex file as hexdump
 - hexdiff - diff dumps of 2 hex files
 - hexinfo - diff dumps of 2 hex files
 - hexmerge -  merge content of hex files

# OPTIONS

Arguments:

*HEXFILE*
name of hex file for processing (use '-' to read from stdin)

**-h, --help**
Display simple help message describing the tool usage.

**-v, --version**
Display hex2dump tool version information

**-r, --range=START:END**
specify address range for dumping (ascii hex value).
Range can be in form 'START:' or ':END'.

**--width=N**
dump N data bytes per line (default: 16).

# HISTORY

January 2019, Man page originally compiled by Philippe Thierry (phil at debian dot org)
